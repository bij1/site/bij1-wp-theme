# BIJ1 WordPress theme

This is a WordPress theme used at [bij1.org](https://bij1.org/),
based on a theme called [Okaia](https://www.okaia.nl/),
and previously maintained by [Multitude](https://www.multitude.nl/).
Changes are initially tested on [wp-staging.bij1.net](https://wp-staging.bij1.net/).

There are a couple of theme settings that you can define as constants:

- LOGO\_BIG\_PATH: url of the big/wide logo
- LOGO\_SMALL\_PATH: url of the small/mobile logo
- IS\_MAIN\_SITE: whether this is bij1.org
- NEWS\_GRID\_VIEW: whether to show the news page as a grid (default = false)

## Todo

- open-source
- [functional demands](https://trello.com/b/h6xl71Se/ict-bij1)
- reverse engineer how the heck their build pipeline worked
- automate/document it
- refactor
- [improve accessibility](https://trello.com/c/KOt0Eq6T/132-toegankelijkheid-verbeteren)
- split off local-friendly version
- format/lint
