<?php /* Template Name: Vacatures */ ?>


<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <?php the_content(); ?>

        <div class="grid-container fluid">
            <div class="grid-x align-center">
                <div class="large-8 cell">
                    <?php 
                        $terms = get_vacature_terms();
                        $posts = get_posts(array(
                            'post_type' => 'vacatures',
                            'numberposts' => -1
                        ));
                        $vacatures_per_term = array();
                        $term_names = array();

                        foreach($terms as $term) {
                            $vacatures_per_term[$term->slug] = array();
                            $term_names[$term->slug] = $term->name;
                        }

                        foreach($posts as $post) {
                            $post_terms = wp_get_post_terms($post->ID, 'soort');
                            $skip = true;
                            foreach($post_terms as $post_term){
                                if(array_key_exists($post_term->slug, $vacatures_per_term)) {
                                    $vacatures_per_term[$post_term->slug][] = $post;
                                    break;
                                }
                            }
                        }

                        foreach($term_names as $term_slug => $term_name) {
                            if(count($vacatures_per_term[$term_slug]) == 0){
                                continue;
                            }
                            echo '<div class="block vacature-type" data-aos="fade-up"> <h2>' . $term_name . '</h2><hr/>';

                            foreach($vacatures_per_term[$term_slug] as $post) {
                            setup_postdata($post);
                    ?>
                                <div class="vacature" data-aos="fade-up">
                                    <div class="grid-x grid-margin-x align-middle">
                                        <div class="auto cell">
                                            <p class="sbm"><strong><?php the_title(); ?></strong></p>
                                            <div class="grid-x grid-margin-x">
                                                <div class="shrink cell">
                                                    <p class="sbm"><?php the_field('dienstverband'); ?></p>
                                                </div>
                                                <div class="shrink cell">
                                                    <p class="sbm"><?php the_field('locatie'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="small-12 medium-shrink cell">
                                            <a href="<?php the_permalink(); ?>" class="button nbm"> <?php if (pll_current_language() == 'en') : ?>Apply<?php else : ?>Solliciteer<?php endif;  ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                </div>
            <?php } ?>


            </div>
        </div>
        </div>

<?php endwhile;
endif; ?>
<div class="spacingblock smallspacingblock"></div>
<?php get_footer(); ?>
