<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   width="40"
   height="40"
   viewBox="0 0 40 40"
   fill="none"
   version="1.1"
   id="svg87"
   sodipodi:docname="menu.svg.php.svg"
   inkscape:version="1.1.1 (c3084ef, 2021-09-22)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <defs
     id="defs91" />
  <sodipodi:namedview
     id="namedview89"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     showgrid="true"
     inkscape:zoom="21.35"
     inkscape:cx="10.796253"
     inkscape:cy="20.023419"
     inkscape:window-width="1860"
     inkscape:window-height="1147"
     inkscape:window-x="60"
     inkscape:window-y="25"
     inkscape:window-maximized="0"
     inkscape:current-layer="svg87">
    <inkscape:grid
       type="xygrid"
       id="grid516" />
  </sodipodi:namedview>
  <circle
     cx="20"
     cy="20"
     r="20"
     fill="white"
     id="circle73" />
  <path
     d="M 11,13 H 29"
     stroke="#161616"
     stroke-width="1.40217"
     stroke-linecap="round"
     id="path75"
     style="stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none" />
  <path
     d="M 11,20 H 29"
     stroke="#161616"
     stroke-width="1.40217"
     stroke-linecap="round"
     id="path77"
     style="stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none" />
  <path
     d="M 11,27 H 29"
     stroke="#161616"
     stroke-width="1.40217"
     stroke-linecap="round"
     id="path79"
     style="stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none" />
</svg>
