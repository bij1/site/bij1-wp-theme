<?php get_header(); ?>

<div class="block 404">
    <div class="grid-container fluid">
        <div class="grid-x align-center text-center">
            <div class="cell">
                <p><br /><br /><br /></p>
                <?php if (pll_current_language() == 'en') : ?>
                    <h1>The requested page could not be found.</h1>
                <?php else : ?>
                    <h1>De opgevraagde pagina is helaas niet gevonden.</h1>
                <?php endif;  ?>
                <p><br /></p>
                <?php if (pll_current_language() == 'en') : ?>
                    <a href="<?php echo site_url(); ?>">Click here to return to the homepage.</a>
                <?php else : ?>
                    <a href="<?php echo site_url(); ?>">Klik hier om terug te keren naar de voorpagina.</a>
                <?php endif;  ?>
                <p><br /><br /><br /></p>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>