<?php

if (function_exists('acf_register_block_type')) {
add_action('acf/init', 'register_acf_block_types');
}

/* SINGLE ITEMS */
function register_acf_block_types()
{
/* SCRAP */
acf_register_block_type(array(
'name' => 'tekst',
'title' => __('Tekst'),
'description' => __('old block dont use'),
'render_template' => 'parts/blocks/tekst.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'readmore',
'title' => __('Readmore'),
'description' => __('Formerly text block, now use only for "read more" expandable text.'),
'render_template' => 'parts/blocks/readmore.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'insight',
'title' => __('Insight / Quote'),
'render_template' => 'parts/blocks/insight.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'nieuws-items',
'title' => __('Nieuws items'),
'render_template' => 'parts/blocks/nieuws_items.php',
'mode' => 'edit',
'icon' => 'list-view'
));

/* SCRAP */
acf_register_block_type(array(
'name' => 'kandidaten',
'title' => __('Kandidaten'),
'render_template' => 'parts/blocks/kandidaten.php',
'mode' => 'edit',
'icon' => 'list-view'
));

acf_register_block_type(array(
'name' => 'programma-items',
'title' => __('Programma items'),
'render_template' => 'parts/blocks/programma_items.php',
'mode' => 'edit',
'icon' => 'list-view'
));

/* SCRAP */
acf_register_block_type(array(
'name' => 'afdelingen-items',
'title' => __('Afdelingen items'),
'render_template' => 'parts/blocks/afdelingen_items.php',
'mode' => 'edit',
'icon' => 'list-view'
));

acf_register_block_type(array(
'name' => 'cta-blocks',
'title' => __('CTA formulieren'),
'render_template' => 'parts/blocks/cta_blocks.php',
'mode' => 'edit',
'icon' => 'megaphone'
));

acf_register_block_type(array(
'name' => 'cta',
'title' => __('CTA'),
'render_template' => 'parts/blocks/cta.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'collectiekaart',
'title' => __('Collectiekaart'),
'render_template' => 'parts/blocks/collectiekaart.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'faq',
'title' => __('FAQ'),
'render_template' => 'parts/blocks/faq.php',
'mode' => 'edit',
'icon' => 'edit'
));

acf_register_block_type(array(
'name' => 'form',
'title' => __('Form'),
'render_template' => 'parts/blocks/form.php',
'mode' => 'edit',
'icon' => 'edit'
));

}

add_filter('allowed_block_types', 'okaia_allowed_block_types');
function okaia_allowed_block_types($allowed_blocks)
{
    return array(
        'acf/tekst',
        'acf/readmore',
        'acf/cta',
        'acf/collectiekaart',
        'acf/faq',
        'acf/insight',
        'acf/nieuws-items',
        'acf/programma-items',
        'acf/afdelingen-items',
        'acf/cta-blocks',
        'acf/kandidaten',
        'acf/form',
        'core/paragraph',
        'core/heading',
        'core/list',
        'core/image',
        'core/quote',
        'core/embed',
        'core/insight'

    );
}

/* STYLING OF GUTHENBERG */
function editor_setup_style()
{

add_theme_support('editor-styles'); // Enqueue editor styles.
/* add_editor_style('css/app.css'); */
}
add_action('after_setup_theme', 'editor_setup_style');



/* CORE BLOCKS THAT ARE VISIBLE IN WORDPRESS */
// ALSO WHAT CONTAINERS WILL BE PUT BEFORE THESE BLOCKS

add_filter('render_block', 'wrap_table_block', 10, 2);
function wrap_table_block($block_content, $block)
{
    if ('core/paragraph' === $block['blockName'] 
    OR 'core/list' === $block['blockName'] 
    OR 'core/heading' === $block['blockName'] 
    OR 'core/image' === $block['blockName'] 

    OR 'core/embed' === $block['blockName']) {
        $block_content = 
        "<div class='block content-block'> <div class='grid-container fluid'> <div class='grid-x align-center'> <div class='large-6 cell'>" . 
        $block_content .
        '</div></div></div></div>';
    }
    elseif ('core/quote' === $block['blockName'] OR 'core/pullquote' === $block['blockName']) {
        $block_content =
            "<div class='block content-block'> <div class='grid-container fluid'> <div class='grid-x align-center'> <div class='large-8 cell'>" .
            $block_content .
            '</div></div></div></div>';
    }
    return $block_content;

}

function gb_gutenberg_admin_styles()
{
echo '
    <style>
        /* Main column width */
        .wp-block {
            max-width: 1080px;
        }

        /* Width of "wide" blocks */
        .wp-block[data-align="wide"] {
            max-width: 1080px;
        }

        /* Width of "full-wide" blocks */
        .wp-block[data-align="full"] {
            max-width: none;
        }
    </style>
';
}

add_action('admin_head', 'gb_gutenberg_admin_styles');
