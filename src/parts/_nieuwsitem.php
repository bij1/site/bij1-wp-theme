                <div class="block nieuwsitem">
                    <div class="grid-container fluid">
                        <div class="grid-x grid-margin-x">

                            <div class="large-1 small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                            else : echo 4;
                                                            endif; ?> cell">
                                <!-- EMPTYCELL -->
                            </div>
                            <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                    else : echo 3;
                                                                    endif; ?>">
                                <?php the_post_thumbnail('large'); ?>
                            </div>
                            <div class="large-5 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                    else : echo 2;
                                                                    endif; ?>">
                                <div class="">
                                    <h3><?php the_title(); ?></h3>
                                    <p><?php the_excerpt(); ?></p>

                                    <a href="<?php the_permalink(); ?>" class="arrowlink"><?php pll_e('Lees meer'); ?></a>
                                </div>
                            </div>
                            <div class="large-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                    else : echo 1;
                                                                    endif; ?>">
                                <!-- EMPTYCELL -->
                            </div>

                        </div>
                    </div>
                </div>