<div class='block text-block'>
    <div class='grid-container fluid'>
        <div class='grid-x align-center'>
            <div class='large-6 cell'>

                <?php if (get_field('read_more')) : ?>

                    <div class="read-more" data-toggler=".show" id="read-more">
                        <?php the_field('tekst'); ?>
                        <div class="white-fade"></div>
                    </div>
                    <div class="grid-container">
                        <div class="grid-x align-center text-center">
                            <div class="shrink cell">
                                <p class="read-more-link link uppercase nbm" id="read-more-text" data-toggler=".hide"><?php pll_e('Lees de introductie'); ?></p>
                                <a class="button whitebutton roundbutton nbm read-more-link" data-toggle="read-more read-more-text read-more-link" id="read-more-link" data-toggler=".hide"><i class="icon-down-open-1"></i></a>

                            </div>
                        </div>
                    </div>

                <?php else : ?>

                    <?php the_field('tekst'); ?>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>