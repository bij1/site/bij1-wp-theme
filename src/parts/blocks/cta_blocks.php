<?php if(isMainSite()): ?> 
<!-- CTA START -->
<?php if (is_single()) : ?>
    <div class="footer-doneer" role="donate form">
        <div class="grid-container fluid">
            <div class="grid-x grid-margin-x">
                <div class="large-12 cell">
                    <p class="nbm tm"><small><?php pll_e('Doneer'); ?></small></p>
                    <hr class="fullwidthline">
                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x grid-margin-x align-center">
                    <div class="large-5 cell">
                        <p><?php the_field('doneer_text', 'options'); ?></p>
                    </div>
                    <div class="large-5 cell">
                        <?php get_template_part('parts/_doneerformulier') ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="footer-nieuwsbrief " role="newsletter subscription">

        <div class="grid-container fluid">
            <div class="grid-x grid-margin-x">
                <div class="large-12 cell">
                    <p class="nbm tm"><small><?php pll_e('Nieuwsbrief'); ?></small></p>
                    <hr class="fullwidthline">
                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x grid-margin-x align-center">
                    <div class="large-5 cell">
                        <p><?php the_field('nieuwsbrief_text', 'options'); ?></p>
                    </div>
                    <div class="large-5 cell">
                        <?php get_template_part('parts/_mailformulier') ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php else : ?>

    <?php if (get_field('type') == 'doneer') : ?>
        <div class="footer-doneer ">
        <?php else : ?>
            <div class="footer-nieuwsbrief ">
            <?php endif; ?>

            <div class="grid-container fluid">
                <div class="grid-x grid-margin-x">
                    <div class="large-12 cell">
                        <?php if (get_field('type') == 'doneer') : ?>
                            <p class="nbm tm"><small><?php pll_e('Doneer'); ?></small></p>
                        <?php else : ?>
                            <p class="nbm tm"><small><?php pll_e('Nieuwsbrief'); ?></small></p>
                        <?php endif; ?>
                        <hr class="fullwidthline">
                    </div>
                </div>
                <div class="blockpadding">
                    <div class="grid-x grid-margin-x align-center">
                        <div class="large-5 cell">

                            <?php if (get_field('type') == 'doneer') : ?>
                                <p><?php the_field('doneer_text', 'options'); ?></p>
                            <?php else : ?>
                                <p><?php the_field('nieuwsbrief_text', 'options'); ?></p>
                            <?php endif; ?>

                        </div>
                        <div class="large-5 cell">
                            <?php if (get_field('type') == 'doneer') : ?>
                                <?php get_template_part('parts/_doneerformulier') ?>
                            <?php else : ?>
                                <?php get_template_part('parts/_mailformulier') ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

            </div>

        <?php endif; ?>
        <!-- CTA EIND -->
<?php endif; ?>
