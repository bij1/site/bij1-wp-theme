<div class='block cta'>
    <div class='grid-container fluid'>
        <div class="grid-x align-center">
            <div class="large-8 cell">
                <div class="inner ">
                    <div class="grid-x grid-margin-x">
                        <div class="large-6 cell">
                            <h2><?php the_field('cta_title'); ?></h2>
                        </div>
                        <div class="large-6 cell">
                            <p><?php the_field('cta_text'); ?></p>
                            <?php
                            $link = get_field('cta_link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a class="button secondary nbm" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>