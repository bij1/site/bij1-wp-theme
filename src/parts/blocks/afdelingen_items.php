<!-- PROGRAMMEBLOCK START -->
<div class="afdelingen_items">
    <div class='titelblock'>
        <div class='grid-container fluid'>
            <div class='grid-x grid-margin-x'>
                <div class='large-12 cell'>

                    <hr class="fullwidthline">

                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x grid-margin-x align-center text-center" data-aos="fade-up">
                    <div class="auto cell"></div>
                    <div class="large-8 cell">
                        <div class="altheader">
                            <h1><?php the_field('title_afdelingen'); ?></h1>

                        </div>
                    </div>
                    <div class="auto cell"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="block itemblock  nospace">
        <div class="grid-container full">
            <?php $postnumber = -1; ?>
            <?php $query = new WP_Query(array('post_type' => 'afdelingen', 'posts_per_page' => $postnumber)); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                    <?php if (get_field('overig', get_the_ID())) : ?>
                    <?php else : ?>
                        <!-- MOBILE LOOP START -->
                        <div class="hide-for-large colorchangeblock <?php if ($query->current_post % 2 == 1) : echo 'blackbackground';
                                                                    else : echo 'whitebackground';
                                                                    endif; ?>">
                            <div class="grid-x ">

                                <div class="large-12  cell">
                                    <?php the_post_thumbnail('large', array('class' => 'coverimage')); ?>
                                </div>
                            </div>
                            <div class="large-12 cell">
                                <div class="mobileinner">

                                    <?php $taxonomy = get_the_terms($post->ID, 'thema'); ?>
                                    <div class="altheader">
                                        <h2>
                                            <span>
                                                <?php the_title(); ?>
                                            </span>
                                        </h2>
                                    </div>
                                    <?php the_excerpt(); ?>


                                    <?php
                                    $link = get_field('website', get_the_ID());
                                    if ($link) : ?>

                                        <a href="<?php echo esc_url($link); ?>" target="_blank" class="<?php if ($query->current_post % 2 == 0) : echo '';
                                                                                                        else : echo 'whitebutton';
                                                                                                        endif; ?> button nbm"><?php pll_e('Naar de site'); ?></a>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <!-- MOBILE LOOP END -->

                        <!-- DESKTOP LOOP START -->
                        <div class="hoverblock show-for-large">
                            <div class="grid-x">
                                <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 1;
                                                                else : echo 2;
                                                                endif; ?> cell">
                                    <!-- EMPTYCELL -->
                                </div>
                                <div class="large-3 small-order-<?php if ($query->current_post % 2 == 0) : echo 2;
                                                                else : echo 3;
                                                                endif; ?> cell">
                                    <div class="flex-container flex-dir-column" style="height: 100%">
                                        <div class="flex-child-grow"></div>
                                        <div class="flex-child-shrink">

                                            <?php $taxonomy = get_the_terms($post->ID, 'thema'); ?>
                                            <div class="altheader">
                                                <h2>
                                                    <span>
                                                        <?php the_title(); ?>
                                                    </span>
                                                </h2>
                                            </div>
                                            <?php the_excerpt(); ?>


                                            <?php
                                            $link = get_field('website', get_the_ID());
                                            if ($link) : ?>
                                                <a href="<?php echo esc_url($link); ?>" target="_blank" class="button nbm"><?php pll_e('Naar de site'); ?></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="flex-child-grow"></div>
                                    </div>
                                </div>
                                <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 3;
                                                                else : echo 4;
                                                                endif; ?> cell">
                                    <!-- EMPTYCELL -->
                                </div>
                                <div class="large-5 small-order-<?php if ($query->current_post % 2 == 0) : echo 4;
                                                                else : echo 1;
                                                                endif; ?> cell">
                                    <div class="imagecontainer ">
                                        <?php the_post_thumbnail('square', array('class' => 'coverimage')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DESKTOP LOOP END -->
                    <?php endif ?>


            <?php endwhile;
                wp_reset_postdata();
            endif; ?>
        </div>
    </div>
    <div class="footerblock block">
        <div class='grid-container fluid'>
            <div class='grid-x grid-margin-x'>
                <div class='large-12 cell'>

                    <hr class="fullwidthline">

                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x grid-margin-x align-center text-center" data-aos="fade-up">
                    <div class="auto cell"></div>
                    <div class="large-8 cell">
                        <div class="altheader">
                            <h1><?php the_field('overige_afdelingen_title'); ?></h1>

                        </div>
                    </div>
                    <div class="auto cell"></div>
                    <div class="shrink cell">
                    </div>
                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x align-center grid-margin-x">
                    <?php $postnumber = -1; ?>
                    <?php $query = new WP_Query(array('post_type' => 'afdelingen', 'posts_per_page' => $postnumber)); ?>
                    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                            <?php if (get_field('overig', get_the_ID())) : ?>
                                <div class="shrink cell">


                                    <div class="headercontainer">
                                        <?php
                                        $website = get_field('website', get_the_ID());
                                        $email = get_field('email', get_the_ID());
                                        if ($website) : ?>
                                            <a href="<?php the_field('website', get_the_ID()); ?>">
                                                <h2> <?php the_title(); ?></h2>
                                            </a>
                                        <?php
                                        elseif ($email) : ?>
                                            <a href="mailto:<?php the_field('email', get_the_ID()); ?>">
                                                <h2> <?php the_title(); ?></h2>
                                            </a>
                                        <?php else : ?>
                                            <h2> <?php the_title(); ?></h2>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            <?php endif ?>

                    <?php endwhile;
                        wp_reset_postdata();
                    endif; ?>
                </div>
            </div>
            <div class="blockpadding">
                <div class="grid-x align-center">
                    <div class="large-6 cell">
                        <p><?php the_field('overige_afdelingen_text'); ?></p>

                        <a class="arrowlink right" href="mailto:<?php the_field('email_afdelingen', 'options'); ?>"><?php pll_e('Mail ons'); ?> <i class="icon-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
<!-- PROGRAMMEBLOCK EIND -->
