<div class='block text-block' id="scrollup">
    <div class='grid-container fluid'>
        <div class='grid-x align-center'>
            <div class='large-6 cell'>

                <?php if (get_field('read_more')) : ?>

                    <div class="read-more" data-toggler=".show" id="read-more">
                        <?php the_field('tekst'); ?>
                        <div class="white-fade"></div>
                    </div>
                    <div class="grid-container">
                        <div class="grid-x align-center text-center">
                            <div class="shrink cell">
                                <div></div>
                                <p class="read-more-link link uppercase nbm" id="read-more-text" data-toggler=".hide"><?php pll_e('Lees de introductie'); ?></p>
                                <button style="cursor: pointer" class="button whitebutton roundbutton nbm read-more-link" data-toggle="read-more read-more-text read-more-link read-more-link-close" id="read-more-link" data-toggler=".hide"><i class="icon-down-open-1"></i></button>
                                <button style="cursor: pointer" class="button whitebutton roundbutton nbm read-more-link hide" data-toggle="read-more read-more-text read-more-link read-more-link-close" id="read-more-link-close" data-toggler=".hide" href="#scrollup"><i class="icon-up-open-1"></i></button>

                            </div>
                        </div>
                    </div>

                <?php else : ?>

                    <?php the_field('tekst'); ?>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>